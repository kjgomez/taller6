#include <stdio.h>
#include <stdlib.h>

char *mesg[2] = {"Par", "impar"} ;

typedef struct estTDA{
        int c;
        int *arr;
        int cant_arr;
        char *mensaje;
}est;

void mostrar_est(est *a,int cant);
void mostrar_int(int *a,int cant);
int *mas(int cant);


int *mas(int cant){
        
        int *b = (int *)malloc (sizeof(int)*cant);

        int i = 0;

        for(i = 0; i < cant; i++){
                b[i]  = i *10;
        }
        return b;

}

void mostrar_est(est *a,int cant){
        int i = 0;
        for(i = 0; i < cant; i++){
                printf("\nEst c: %d, mensaje: %s\n", (a+i)->c, (a+i)->mensaje);
                mostrar_int((a+i)->arr,i);
                printf("\n");
        }

}

void mostrar_int(int *a,int cant){
        int i = 0;
        for(i = 0; i < cant ; i++){
                printf("%d\n", *(a+i));
        }
}


int *fn1(int cant, int mul){
        int *buf= (int *) malloc (sizeof(int)*cant);
        int i = 0;
        for(i = 0; i < cant; i++){
                buf[i] = mul*i;
        }
	return buf;
 }

void fn2(int cant, est **res){
	est *data = *res;

        if(data == NULL){
                return;
        }


        int i = 0;
        for(i = 0; i < cant ; i++){
                (data+i)->c = i;
                data[i].arr = fn1(i,3);
                data[i].cant_arr = i;
                (data+i)->mensaje = mesg[ i % 2 ];
		
        }
        
}

int main(int argc, char **argv){

        if(argc < 2){
                printf("Uso: ./progra <numero de elementos>\n");
                exit(EXIT_FAILURE);
        }

        int cant = atoi(argv[1]);
        if(cant < 10){
                printf("Argumento debe ser al menos 10\n");
                exit(EXIT_FAILURE);
        }


        int *arr = fn1(cant, 2);
        est *res = (est *) malloc(sizeof(est)*cant);
        fn2(cant, &res);
        mostrar_int(arr,cant);
        int *arr2 = mas(cant);
        mostrar_est(res,cant);
        mostrar_int(arr2,cant);
	free(arr);
	free(arr2);
	est *data= res;
	int i=0;
	for(i=0; i<cant;i++){
		free(data[i].arr);
	}
	free(data);
        exit(EXIT_SUCCESS);
	
}

